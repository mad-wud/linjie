module gitee.com/mad-wud/linjie

go 1.20

require (
	gitee.com/mad-wud/lin v1.3.95
	github.com/360EntSecGroup-Skylar/excelize v1.4.1
	github.com/atotto/clipboard v0.1.4
	github.com/dop251/goja v0.0.0-20231027120936-b396bb4c349d
	github.com/sqweek/dialog v0.0.0-20220809060634-e981b270ebbf
)

require (
	github.com/TheTitanrain/w32 v0.0.0-20180517000239-4f5cfb03fabf // indirect
	github.com/dlclark/regexp2 v1.7.0 // indirect
	github.com/go-sourcemap/sourcemap v2.1.3+incompatible // indirect
	github.com/google/pprof v0.0.0-20230207041349-798e818bf904 // indirect
	github.com/mohae/deepcopy v0.0.0-20170929034955-c48cc78d4826 // indirect
	golang.org/x/text v0.3.8 // indirect
)
