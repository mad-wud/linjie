package linjie

import (
	"errors"

	"github.com/dop251/goja"
)

type Gojs struct {
	js *goja.Runtime
}

func (类 *Gojs) C初始化创建(js代码 string) error {
	类.js = goja.New()
	_, err := 类.js.RunString(js代码)
	return err
}

//@___独立方法 和 类无关联
func (Gojs) C创建并执行(js代码 string) (执行结果 goja.Value, err error) {
	js := goja.New()
	执行结果, err = js.RunString(js代码)
	return
}

// @___定义的go方法指针  =  func(参数 any) (返回值 any)  any 可以是任意基础类型
// @___传参 用   &定义的go方法指针
func (类 *Gojs) Q取出方法(js方法名 string, 定义的go方法指针 interface{}) error {
	if 类.js == nil {
		err := errors.New("gojs 对象 未初始化")
		return err
	}
	err:=类.js.ExportTo(类.js.Get(js方法名), 定义的go方法指针)

	return err
}


