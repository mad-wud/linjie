package linjie

import (
	"github.com/atotto/clipboard"

	"github.com/sqweek/dialog"
)

type X系统 struct {
}

func (X系统) J剪贴板_写入文本(写入文本 string) error {
	return clipboard.WriteAll(写入文本)
}

func (X系统) J剪贴板_读出文本() (返回文本 string, err error) {
	返回文本, err = clipboard.ReadAll()
	return
}

// 类型选择_空为普通_警告或选择   1 = '警告'  2=选择  其他或者空 为普通信息框
func (X系统) X信息框(标题, 正文 string, 类型选择_空为普通_1警告_2选择 ...int) (返回选择结果 bool) {
	if len(类型选择_空为普通_1警告_2选择) == 0 {

		dialog.Message(正文).Title(标题).Info()
	} else if 类型选择_空为普通_1警告_2选择[0] == 1 {
		dialog.Message(正文).Title(标题).Error()
	} else if 类型选择_空为普通_1警告_2选择[0] == 2 {
		返回选择结果 = dialog.Message(正文).Title(标题).YesNo()
	} else {
		dialog.Message(正文).Title(标题).Info()
	}
	return

}

