package linjie

import (
	"fmt"
	"testing"
)

func Test_G0js(t *testing.T) {
	//例子1
	var js Gojs
	结果, err := js.C创建并执行("1+1")
	fmt.Println(err)
	答案1 := 结果.ToFloat()
	fmt.Println("例子1结果",答案1, 结果.ToBoolean(), 结果.String())

	//例子2
	var js2 Gojs
	js2.C初始化创建(`
	function 无敌(n){
		let 哈哈={
			aaa:n,
			bbb:2
		}
		return 哈哈
	}
	`)
	var 绑定方法 func(参数1 any) any
	js2.Q取出方法("无敌", &绑定方法)
	fmt.Println("例子2结果",绑定方法(666))

	//例子3
	var js3 Gojs
	js3.C初始化创建(`
	function 无敌(n,cc){
		let 哈哈={
			aaa:n,
			bbb:2,
			哈哈:cc
		}
		return 哈哈
	}
	`)
	var 绑定方法2 func(参数1, 参数2 any) map[string]any
	js3.Q取出方法("无敌", &绑定方法2)
	fmt.Println("例子3结果",绑定方法2(666, 77))

}


func Test_G0js2(t *testing.T) {
	//案例1
	var js Gojs
	结果,err:=js.C创建并执行(`
	function 无敌(){
		let a ={
			哈哈:111,
			呵呵:222
		}
		return a.呵呵
	}
	无敌()
	
	`)
	fmt.Println(结果.String())
	fmt.Println(结果.ToFloat())
	fmt.Println(结果,err)

	//案例2 
	js.C初始化创建(`
	function 无敌(n){
		let a ={
			哈哈:111,
			呵呵:222,
			我是N:n
		}
		return a
	}
	`)
	var 绑定方法 func(参数1 int)map[string]any
	js.Q取出方法("无敌",&绑定方法)
	fmt.Println("案例2 ",绑定方法(66))

	//案例3
	js.C初始化创建(`
	function 无敌(n,cc,dd){
		let a ={
			哈哈:111,
			呵呵:222,
			我是N:n,
			我是cc:cc,
			我是dd:dd

		}
		return a
	}
	`)
	var 绑定方法2 func(参数1,参数2 int,参数3 string)map[string]any
	js.Q取出方法("无敌",&绑定方法2)
	fmt.Println("案例2 ",绑定方法2(66,77,"你好"))

}