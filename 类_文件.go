package linjie

import (
	"github.com/sqweek/dialog"
)

type W文件 struct {
}

// @ 是否保存模式 true 是保存模块 false 是打开模式
func (W文件) DK打开文件选择器(是否保存模式 bool,文件类型说明 string,文件类型筛选条件 ...string ) (返回文件路径 string,err error) {
	if 是否保存模式{
		返回文件路径,err= dialog.File().Filter(文件类型说明,文件类型筛选条件...).Save()
	}else{
		返回文件路径,err= dialog.File().Filter(文件类型说明,文件类型筛选条件...).Load()
	}
 return
}
func (W文件) DK打开文件夹浏览器(浏览器标题 string ) (返回文件路径 string,err error) {
	返回文件路径, err = dialog.Directory().Title(浏览器标题).Browse()
 return
}